Description
==============

**Bellamy, C., Boughey, K., Hawkins, C. et al. A sequential multi-level framework to improve habitat suitability modelling. Landscape Ecol (2020). https://doi.org/10.1007/s10980-020-00987-w**
  Open access, available here: https://rdcu.be/b3lv0 
  Please acknowledge this paper when using the code or data outputs


Bellamy_et_al_Sequential_HSM_code.Rmd
--------------
This R code is provided to facilitate the implementation of the sequential, multi-level habitat suitability modelling framework described Bellamy et al., (In Review).

It focuses on steps 4 and 5 of the framework for one level; it is assumed that some of the data preparation involved in steps 1-3 has already been completed. The code should be re-run for each level in turn in hierarchical order. Update folder pathways and file names as required.


LesserHorseshoeHSMoutput
--------------
Raster files providing output predicted habitat suitability indices for *R. hipposideros*, as described in Bellamy et al. (In Review):

  - population range (GB)
  - summer range (GB)
  - local roosting habitat (study area)
  - local habitat habitat (study area)



Corresponding author: 
--------------
Chloe Bellamy **chloe.bellamy@forestresearch.gov.uk**

Please get in touch with questions about the code or data.

For more information about the wider 'Putting UK woodland bats on the map' project, please see: https://www.forestresearch.gov.uk/research/putting-uk-woodland-bats-on-the-map/ 


